<?php

/**
 * Implements hook_rules_event_info().
 */
function switips_ubercart_rules_event_info() {
  $order_arg = array(
    'type' => 'integer',
    'label' => t('Order'),
  );
  
  $events['switips_ubercart_event_update_order'] = array(
    'label' => t('Switips event - order update'), // Switips событие - обновление заказа
    'group' => t('Order'),
    'variables' => array(
      'order' => $order_arg,
    ),
  );

  $events['switips_ubercart_order_delete'] = array(
    'label' => t('Switips event - order deletion'), // Switips событие - удаление заказа
    'group' => t('Order'),
    'variables' => array(
      'order' => $order_arg,
    ),
  );

  return $events;
}

/**
 * Implements hook_rules_action_info().
 */
function switips_ubercart_rules_action_info() {
  $order_arg = array(
    'type' => 'integer',
    'label' => t('Order'),
  );

  $actions['switips_ubercart_new_order'] = array(
    'label' => t('Send data to Switips (new order)'), // Отправить данные в Switips(новый заказ)
    'group' => t('Order'),
    'base' => 'switips_ubercart_new_order',
    'parameter' => array(
      'order' => array(
        'type' => 'uc_order',
        'label' => t('Order'),
    ),
    ),
  );
  
  $actions['switips_ubercart_action_update_order'] = array(
    'label' => t('Send data to Switips (order update)'), // Отправить данные в Switips(обновление заказа)
    'group' => t('Order'),
    'base' => 'switips_ubercart_update_order',
    'parameter' => array(
      'order' => $order_arg,
    ),
  );  
  
  $actions['switips_ubercart_action_order_delete'] = array(
    'label' => t('Send data to Switips (order deletion)'), // Отправить данные в Switips(удаление заказа)
    'group' => t('Order'),
    'base' => 'switips_ubercart_order_delete',
    'parameter' => array(
      'order' => $order_arg,
    ),
  ); 
  
  return $actions;
}
