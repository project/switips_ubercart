<?php

/**
 * @file
 * Default rules configurations.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function switips_ubercart_default_rules_configuration() {
  $configs = array();

  $rule = rules_reaction_rule();
  $rule->label = t('Switips. New order'); // Switips. Новый заказ
  $rule->active = TRUE;
  $rule->event('uc_checkout_complete', []);
  $rule->action('switips_ubercart_new_order', []);
  $configs['switips_ubercart_rule_order_new'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = t('Switips. Updating order'); // Switips. Обновление заказа
  $rule->active = TRUE;
  $rule->event('switips_ubercart_event_update_order', []);
  $rule->action('switips_ubercart_action_update_order', []);
  $configs['switips_ubercart_rule_order_update'] = $rule;
  
  $rule = rules_reaction_rule();
  $rule->label = t('Switips. Deleting an order'); // Switips. Удаление заказа
  $rule->active = TRUE;
  $rule->event('switips_ubercart_order_delete', []);
  $rule->action('switips_ubercart_action_order_delete', []);
  $configs['switips_ubercart_rule_order_delete'] = $rule;

  return $configs;
}


