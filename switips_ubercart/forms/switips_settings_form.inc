<?php

function switips_settings_form($form, &$form_state){

  $switips = variable_get('switips_settings');
  $vocabulary = taxonomy_get_vocabularies();
  
  $switipsObj = new Switips('empty');
  $switips['api_url'] = isset($switips['api_url']) ? $switipsObj->getDefaultDomain($switips['api_url']) : $switipsObj->getDefaultDomain();
  variable_set('switips_settings', $switips);

  $form['status_active'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable module'), // Включить модуль
    '#default_value' => isset($switips['status_active']) ? $switips['status_active'] : '',        
  ]; 
  $form['api_url'] = [
    '#type' => 'textfield',
    '#title' => t('URL for requests to switips'), // URL для запросов к switips
    '#default_value' => $switips['api_url'],
  ];
  $form['secret_key'] = [
    '#type' => 'textfield',
    '#title' => t('API key'), // Ключ API
    '#default_value' => isset($switips['secret_key']) ? $switips['secret_key'] : '',
    // (secret_key) Доступен в личном кабинете office.switips.com в разделе Настройки — Для разработчика
    '#description' => t('(secret_key) Available in your office.switips.com personal account in the Settings - For developer'),
  ];
  $form['merchant_id'] = [
    '#type' => 'textfield',
    '#title' => t('Partner ID'), // Идентификатор Партнера
    '#default_value' => isset($switips['merchant_id']) ? $switips['merchant_id'] : '',
    // (merchant_id) Присваивается компанией и остается статичным
    '#description' => t('(merchant_id) Assigned by the company and remains static'),
  ];

  $default_catalog = isset($switips['catalog']) ? $switips['catalog'] : '';
  $form['catalog'] = [
    '#type' => 'select',
    '#title' => t('Product Catalog'), // Каталог продукции
    '#options' => getFieldList(),
    '#default_value' => [$default_catalog],
    // Выберите поле, отвечающее в товаре за каталог
    '#description' => t('Select the field responsible for the catalog in the product'),
  ];      

  $default_vocabulary = isset($switips['vocabulary']) ? $switips['vocabulary'] : 'empty';
  $form['commission'] = array(
    '#type' => 'fieldset',
    '#title' => t('Agent s commission'), // Агентское вознаграждение
    '#open' => false,
    // Для расчета агентского вознаграждения, нужно добавить соответствующие поля в категории товара и в товар.<br>Укажите, какой словарь таксономии содержит категории товара. Так же нажмите на галочку Добавить поле с агентским вознаграждением в товар
    '#description' => t('To calculate the agency fee, you need to add the appropriate fields in the product category and in the product. <br> Indicate which taxonomy dictionary contains product categories. Also click on the checkbox Add a field with an agent s fee to the product'),
  );
    $form['commission']['vocabulary'] = [
      '#type' => 'select',
      '#options' => getOptions($vocabulary),
      '#default_value' => [$default_vocabulary],
    ]; 
    $form['commission']['product_add_field'] = [
      '#type' => 'checkbox',
      '#title' => t('Add an agent fee field to a product'), // Добавить поле с агентским вознаграждением в товар
      '#default_value' => isset($switips['product_add_field']) ? $switips['product_add_field'] : '',        
    ]; 
  $form['submit'] = [
    '#type' => 'submit',
    '#name' => 'submit',
    '#value' => t('Save'), // Сохранить
  ];

	return $form;	
}

function switips_settings_form_validate($form, &$form_state) {
 
}

function switips_settings_form_submit($form, &$form_state){

  if($form_state['values']['api_url'] == ''){
    $switipsObj = new Switips('empty');
    $form_state['values']['api_url'] = $switipsObj->getDefaultDomain();
  }

  variable_set('switips_settings', $form_state['values']);

  $field_switips_commission_t = field_info_field('field_switips_commission_t');
  if(is_null($field_switips_commission_t) && $form_state['values']['vocabulary'] != 'empty'){
    addCommissionFieldsTaxonomy($form_state['values']['vocabulary']);
  }
  
  $field_switips_commission_p = field_info_field('field_switips_commission_p');
  if(is_null($field_switips_commission_p) && $form_state['values']['product_add_field'] > 0){
    addCommissionFieldsProduct();
  }
  
	return $form;
}

function getOptions($vocabularies){
  $output = ['empty'=>t('Not chosen')]; // Не выбрано
  foreach($vocabularies as $vocabulary){
    $output[$vocabulary->machine_name] = $vocabulary->name;
  }
  
  return $output;
}

function getFieldList(){
  $output = ['empty'=>t('Not chosen')];
  $fields = field_info_instances('node', 'product');
  
  if(is_array($fields)){
    foreach($fields as $key=>$value){
      $output[$key] = $value['label'];
    }
  }
  
  return $output;
}

function addCommissionFieldsTaxonomy($bundle){
  $field = [
    'field_name' => 'field_switips_commission_t',
    'type' => 'text',
    'settings' => [
      'max_length' => 40,
    ],    
  ];
  field_create_field($field);

  // Сумма агентского вознаграждения передается в Switips.<br>При указании знака %, сумма вознаграждения будет рассчитана, как % от стоимости товара<br>Пример: <b>10%</b>
  $instance = [
    'label' => t('Agent s commission'),
    'description' => t('The amount of the agent s fee is transferred to Switips. <br> If you indicate the% sign, the amount of the fee will be calculated as a% of the value of the product <br> Example: <b> 10% </b>'),  
    'field_name' => $field['field_name'],
    'entity_type' => 'taxonomy_term',
    'bundle' => $bundle,
    'langcode' => 'en',
    'widget' => [
      'settings' => [
        'size' => 40,
      ],
    ],
    'display' => [
      'default' => [
        'type' => 'hidden',
      ],
    ],    
  ];
  field_create_instance($instance);  
}

function addCommissionFieldsProduct(){
  $field = [
    'field_name' => 'field_switips_commission_p',
    'type' => 'text',
    'settings' => [
      'max_length' => 40,
    ],    
  ];
  field_create_field($field);

  $instance = [
    'label' => t('Agent s commission'),
    'description' => t('The amount of the agent s fee is transferred to Switips. <br> If you indicate the% sign, the amount of the fee will be calculated as a% of the value of the product <br> Example: <b> 10% </b>'),  
    'field_name' => $field['field_name'],
    'langcode' => 'en',
    'entity_type' => 'node',
    'bundle' => 'product',
    'widget' => [
      'settings' => [
        'size' => 40,
      ],
    ],
    'display' => [
      'default' => [
        'type' => 'hidden',
      ],
    ],    
  ];
  field_create_instance($instance);
}

 